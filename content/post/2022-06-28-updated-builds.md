---
title: Updated builds
date: 2022-06-28
comments: false
authors:
 - illwieckz
image: /img/screenshots/20220628-201229-000.netradiant-packages.png
---

![NetRadiant packages](/img/screenshots/20220628-201229-000.netradiant-packages.png)

Builds were updated, see the [download page](/page/download)!

They're built for Linux, Windows (32 & 64-bit), macOS and FreeBSD. They have native or native-like apperance on all systems and macOS application supports both Light and Dark theme (NetRadiant selects the right one at sartup).

Xonotic mapping support package is not required anymore: [in-PK3 symlinks](/post/2021-02-25-pk3-symlink/) are now supported as well as [`dds/` prefix](/post/2021-11-08-dds-prefix/). Q3map2 [can bake IQM models](/post/2021-08-10-iqm-q3map2/), NetRadiant camera [field of view is modifiable](/post/2021-06-27-modifiable-fov/), and Linux NetRadiant binary [is clickable](/post/2021-03-07-clickable-linux-binary/).

Some settings folder path [have changed](/post/2022-06-19-user-path-change/).

Some [repositories were created](https://gitlab.com/netradiant/gamepacks) for some game packs, if you're a contributor of such games or want to help refining the experience for your favorite game, you can help!

Support [for macOS](/post/2021-02-20-macos-debut/) is new and still rough. There may be problems with multi-screen configurations.
