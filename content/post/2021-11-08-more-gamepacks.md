---
title: More game packs
date: 2022-06-19
comments: false
authors:
 - illwieckz
image: /img/screenshots/20220628-190940-000.netradiant-gamepacks.png
---

![NetRadiant gamepacks](/img/screenshots/20220628-190940-000.netradiant-gamepacks.png)

More games packs are now shipped, games like Warfork, AlienArena, UrbanTerror, Quake Live, Soldier of Fortune 2, Star Trek Voyager: Elite Force… Some [repositories](https://gitlab.com/netradiant/gamepacks) were created to store some fixes for them.
