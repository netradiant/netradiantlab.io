---
title: "New game packs: Q3Rally, Smokin' Guns and Kingpin"
date: 2019-05-18
comments: false
authors:
 - illwieckz
image: /img/screenshots/20200120-175656-000.netradiant-q3rally.png
---

![NetRadiant editing a Q3Rallu map](/img/screenshots/20200120-175656-000.netradiant-q3rally.png)

Some new gamepacks were added for three games: [Q3Rally](http://www.q3rally.com/), [Smokin' Guns](https://www.smokin-guns.org/), and [Kingpin](https://kingpin.info/).

In order to add support for the Smokin' Guns game, some features from an old Smokin' Guns fork of Q3map2 (from _SG Mapping tools_) had to be merged first.

See related merge threads for more information: [Q3Rally game pack](https://gitlab.com/xonotic/netradiant/merge_requests/121), [Smokin' Guns support](https://gitlab.com/xonotic/netradiant/merge_requests/122), [Kingpin game pack](https://gitlab.com/xonotic/netradiant/merge_requests/149).
