---
title: IQM models in q3map2
date: 2021-08-10
comments: false
authors:
 - illwieckz
image: /img/screenshots/20210729-224125-000.unvanquished-iqm-baked-model.jpg
---

![Unvanquished IQM baked model](/img/screenshots/20210729-224125-000.unvanquished-iqm-baked-model.jpg)

The q3map2 map compiler now supports IQM models and can bake them in BSP when compiling maps.

Until now, only the NetRadiant editor had IQM model support, the editor was able to render them but to get such model included in a map one had to use them as `misc_anim_model` to let the engine render them, and it was not possible to compute lightmaps for them.

Now that the map compiler also supports this model format, it's possible to bake such IQM model in geometry and compute lightmaps for them.

The code was ported by eukara from the [WorldSpawn editor](https://github.com/VeraVisions/worldspawn) (another NetRadiant fork). While the [previous IQM code](/post/2018-04-29-iqm-model/) was a NetRadiant plugin ported from AARadiant, this new IQM code lives in `picomodel` library which is used by both the editor and the map compiler. IQM animation are not supported but that's good enough!

That code was first ported to [GtkRadiant](https://github.com/TTimo/GtkRadiant/pull/668) and then ported to [NetRadiant](https://gitlab.com/xonotic/netradiant/-/merge_requests/184) and [DarkRadiant](https://github.com/codereader/DarkRadiant/pull/17)! Go go IQM!
