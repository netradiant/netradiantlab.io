---
title: PK3dir and DPK/DPKdir support
date: 2017-07-10
comments: false
authors:
 - illwieckz
image: /img/screenshots/20200120-023353-000.nautilus-xonotic-pk3dir.png
---

![Xonotic PK3dirs](/img/screenshots/20200120-023353-000.nautilus-xonotic-pk3dir.png)

PK3dir and DPKdir support was added. A PK3dir is a directory with the exact same layout of a PK3, to be used in place of PK3 for development purpose.
A DPKdir is the same kind of directory but for DPK archives. The DPK format extends the PK3 one with dependency management. At this point, only NetRadiant handles dependencies, The Q3map2 map compiler loads all DPK as PK3 without taking care of dependencies yet.

Part of this feature was also implemented on [GtkRadiant](https://icculus.org/gtkradiant/) side: like Q3map2, DPKdir are loaded like PK3dir. On [DarkRadiant](https://www.darkradiant.net) side it was discovered that other editor did not require any patch since its design already supports such mechanism for arbitrary package extension as long as this package uses PKZIP compression.

See merge threads about [implementation on NetRadiant](https://gitlab.com/xonotic/netradiant/merge_requests/39), [implementation on GtkRadiant](https://github.com/TTimo/GtkRadiant/pull/533), and [some fixes](https://gitlab.com/xonotic/netradiant/merge_requests/81).
