---
title: CSG Make Room operation
date: 2018-01-06
comments: false
authors:
 - illwieckz
image: /img/screenshots/20180304-053742-000.netradiant-csg-make-room.png
---

![CSG Make Room](/img/screenshots/20180304-053742-000.netradiant-csg-make-room.png)

With the help of Garux (thanks to him!), the CSG “_Make Room_” operation was added.

It makes possible to create a room from a brush by surrounding it, which is very handy to create skyboxes. See the related [merge request thread](https://gitlab.com/xonotic/netradiant/merge_requests/88) for details about implementation.

The feature [was also ported](https://bugs.thedarkmod.com/view.php?id=4747) to [DarkRadiant](https://www.darkradiant.net/) to help our friends from The Dark Mod and the Doom 3 mapping community.
