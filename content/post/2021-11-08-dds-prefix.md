---
title: DDS prefix
date: 2021-11-08
comments: false
authors:
 - illwieckz
image: /img/screenshots/20220628-185716-000.netradiant-xonotic-finalrage.png
---

![Xonotic Final Rage map in NetRadiant](/img/screenshots/20220628-185716-000.netradiant-xonotic-finalrage.png)

Games like Doom3 and Xonotic expect a `dds/` prefix for DDS images. For example a TGA image named `textures/castle/brick.tga` would be stored as `dds/textures/castle/brick.dds` when distributed in DDS format. The feature to also look for a given image path in `dds/` folder was implemented both in NetRadiant level editor and the q3map2 map compiler.

DDS format is DirectDraw Surface, a compressed image format tailored for GPUs (graphics card can process them compressed in memory). For some reasons Doom 3 stored images in such format into a specific folder, and Darkplaces engine (powering Xonotic) did the same, requiring every tool to implement the trick.

This was the remaining thing to implement to be able to ditch the Xonotic mapping package. Now Xonotic mappers can edit maps against released Xonotic data without needing an extra package. The other big change to achieve that was the implementation of [in-PK3 symlink](/post/2021-02-25-pk3-symlink/), also implemented earlier this year.

Note: the `dds/` prefix is very annoying to deal with and brings nothing good in exchange. If you're making a new game engine, better avoid requiring this prefix.
