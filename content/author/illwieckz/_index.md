---
title: Thomas Debesse
subtitle: illwieckz
name: illwieckz
comments: false
---

Thomas “illwieckz” Debesse is a core developer of the [Unvanquished game](https://unvanquished.net) and contributes a lot to NetRadiant!
He is also building some NetRadiant packages, see the [Download page](/page/download).


You can give Thomas Debesse time to work on NetRadiant:

{{< block >}}

[![Donate on Patreon](https://img.shields.io/badge/donate-patreon-red?style=for-the-badge&logo=patreon#inline)](https://www.patreon.com/bePatron?u=29259270#quiet)
[![Donate on LiberaPay](https://img.shields.io/badge/donate-liberapay-yellow?style=for-the-badge&logo=liberapay#inline)](https://liberapay.com/illwieckz/donate#quiet)
[![Donate on Tipeee](https://img.shields.io/badge/donate-tipeee-e5555a?style=for-the-badge&logo=githubsponsors#inline)](https://fr.tipeee.com/illwieckz/#quiet)
[![Donate on Paypal](https://img.shields.io/badge/donate-paypal-blue?style=for-the-badge&logo=paypal#inline)](https://www.paypal.me/illwieckz#quiet)

{{< /block >}}

More information on the [Patreon](https://www.patreon.com/illwieckz), [Liberapay](https://liberapay.com/illwieckz/) and [Tipeee](https://fr.tipeee.com/illwieckz) pages.
