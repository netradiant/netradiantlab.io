---
title: Team Xonotic
subtitle: The Arena game shooter
name: xonotic
comments: false
authors:
 - xonotic
---

Xonotic is an addictive free and fast arena shooter game, [check it out](https://xonotic.org)! NetRadiant is the recommended editor for the game.

Team Xonotic is developing the Xonotic game the open source way and is maintaining NetRadiant as well.

The [NetRadiant repository](https://gitlab.com/xonotic/netradiant) is hosted as a subproject on [Xonotic's GitLab](https://gitlab.com/xonotic).
