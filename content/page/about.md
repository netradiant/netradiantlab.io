---
title: About NetRadiant
subtitle: This is a piece of history
comments: false
authors:
 - xonotic
image: /img/netradiant-splash.png
---

![NetRadiant splash](/img/netradiant-splash.png#large)

NetRadiant is a free and open-source 3D game level editor using the brush methodology.

NetRadiant is cross platform and is known to run well on Linux, Windows and FreeBSD. While not being native on macOS yet, it is reported to run well on this system using Wine. [**Download NetRadiant**](/page/download) and start mapping!

Historically a fork of the GtkRadiant project from Id Software, it is maintained by a community of gamers, mappers, modders and game developpers.

NetRadiant development is driven by [Xonotic](https://xonotic.org) and is getting love from [Unvanquished](https://unvanquished.net).

NetRadiant supports many other games! Check it out!
